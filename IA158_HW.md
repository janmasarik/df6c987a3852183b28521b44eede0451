# sPyd3r
Lego MINDSTORMS EV3 SUMO robot

IA158 Real Time systems homework

###  Task performed

* created robot from a scratch
* flashed ev3dev debian based linux image to sd card and then booted it
* implemented functionality of the robot in python
* showed demo of functionality

### Implementation

This was quite easy part. `ev3dev` provides very [detailed guide how to get started](http://www.ev3dev.org/docs/getting-started/). Then we followed [this nice tutorial](https://sites.google.com/site/ev3python/learn_ev3_python) and tried some basics through interactive python3 shell.

After that we implemented desired functionality into python script and tested it.

There were some small issues / gotchas:

* in case you have color sensor too close to the ground, it will report false positives
* angle of infra sensor is quite important

Overally we were really satisfied with `ev3dev` project and would definitely recommend it over leJOS.

### Deviations from the abstract

* We used `ev3dev` instead of leJOS since we prefer Python over Java. `ev3dev` is also more active in recent years than leJOS. 
* Otherwise none. We used 3 sensors, 2 motors, 2 threads (including the main one).

### Use of concurrency and timing

We used concurrency to check for black line which specifies playground and for safe turn-off. Concurrency was also used without explicit threads.

Timing was used to aggresive charge or turning of the robot.

### Team work
* we met together and did it together in person (3 afternoons)

### [Source code](https://gist.github.com/janmasarik/4a5e4e69028c7a1fa7d621c91f6486d2)